/* File: gulpfile.js */

// Helpers
var helper = require('./gulpfile_helpers');

// Dependencies
var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	inject = require('gulp-inject-string'),
	webserver = require('gulp-webserver'),
	concat = require('gulp-concat'),
	jscs = require('gulp-jscs'),
	Server = require('karma').Server,
	pug = require('gulp-pug'),
	jsonlint = require('gulp-jsonlint'),
	remark = require('gulp-remark'),
	webdriver = require('gulp-webdriver'),
	pugLinter = require('gulp-pug-linter'),
	package = require('./package.json'),
	pugLintStylish = require('puglint-stylish');

var TOOLS = ['*.conf.js', 'gulpfile.js', 'gulpfile_helpers.js', 'scripts/*.js', 'js/examples/*.js'];
var UNIT_TESTS = ['spec/**/*.spec.js', 'spec/support/*.js'];
var UI_TESTS = ['spec-e2e/**/*.spec.js', 'spec-e2e/support/*.js'];
var SOURCE = ['js/helpers/constants.js', 'js/helpers/misc.js',
	'js/helpers/formatters.js', 'js/helpers/commandHandlers.js',
	'js/helpers/github.js',
	'js/cmd-resume.js'
];
var JS_SOURCE = ['js/cmd-resume.js', 'js/helpers/*.js'];
var OUTPUT = ['tmp/js/cmd-resume.js'];
var JSON = ['browserstack/*.json', 'fixtures/*.json', 'responses/*.json',
	'fixtures/*.json', 'spec/.jscsrc*', '.jshintrc-*', '..jscsrc-*',
	'spec/.jshintrc', 'spec-e2e/.jscsrc', 'spec-e2e/.jshintrc',
	'package.json', 'package-lock.json', 'lint-staged.config.json'
];
var MARKDOWN = ['docs/**/*.mdpp', 'LICENSE.md'];
var PUG = ['templates/**/*.pug'];

var files = helper.getArgumentList(process.argv);
files = files.length ? files : false;

var OPERATING_SYSTEM = helper.getCurrentOperatingSystem();

function compiledCode(destination, minified, versioned) {
	var stream = gulp.src(SOURCE)
		.pipe(concat(minified ? 'cmd-resume.min.js' : 'cmd-resume.js'));

	stream.pipe(inject.prepend(';(function($){\n"use strict";\n\n'))
		.pipe(inject.afterEach('\n', '  '))
		.pipe(inject.append('\n}(jQuery));'));

	if (minified) {
		stream.pipe(uglify());
	}

	if (versioned) {
		stream.pipe(inject.prepend(helper.getVersionString(package)));
	}

	return stream.pipe(gulp.dest(destination));
}

function getE2EBrowsers(browserList, headless, server) {
	var capabilities = [];

	headless = headless ? headless : false;
	server = server ? server : false;

	browserList.forEach(function(browser) {
		var capability = {};

		if (headless && browser === 'firefox') {
			capability['moz:firefoxOptions'] = {
				args: ['-headless']
			};

			if (server) {
				capability['moz:firefoxOptions'].binary = '/usr/bin/firefox';
			}
		} else if (headless && browser === 'chrome') {
			capability.version = 71;
			capability.chromeOptions = {
				args: ['--headless', '--disable-gpu']
			};

			if (server) {
				capability.chromeOptions.binary = '/usr/bin/google-chrome';
			}
		}

		capability.browserName = browser;

		capabilities.push(capability);
	});

	return capabilities;
}

function serve() {
	gulp.src('./')
		.pipe(webserver({
			port: process.env.PORT || 5000, // localhost:5000
			livereload: true
		}));
}

function jsHintDevelopment(done) {
	gulp.src(files ? files : OUTPUT)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(jshint.reporter('fail'));
	done();
}

function jshintTools(done) {
	gulp.src(files ? files : TOOLS)
		.pipe(jshint('./.jshintrc-tools'))
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(jshint.reporter('fail'));
	done();
}

function jshintUnitTests(done) {
	gulp.src(files ? files : UNIT_TESTS)
		.pipe(jshint('./spec/.jshintrc'))
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(jshint.reporter('fail'));
	done();
}

function jshintUITests(done) {
	gulp.src(files ? files : UI_TESTS)
		.pipe(jshint('./spec-e2e/.jshintrc'))
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(jshint.reporter('fail'));
	done();
}

var jshintTests = gulp.parallel(jshintUnitTests, jshintUITests);

function jscsDevelopment(done) {
	gulp.src(files ? files : SOURCE)
		.pipe(jscs())
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'));
	done();
}

function jscsTools(done) {
	gulp.src(files ? files : TOOLS)
		.pipe(jscs({configPath: './.jscsrc-tools'}))
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'));
	done();
}

function jscsUnitTests(done) {
	gulp.src(files ? files : UNIT_TESTS)
		.pipe(jscs({configPath: './spec/.jscsrc'}))
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'));
	done();
}

function jscsUITests(done) {
	gulp.src(files ? files : UI_TESTS)
		.pipe(jscs({configPath: './spec-e2e/.jscsrc'}))
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'));
	done();
}

var jscsTests = gulp.parallel(jscsUnitTests, jscsUITests);

function jsonLint(done) {
	gulp.src(files ? files : JSON)
		.pipe(jsonlint())
		.pipe(jsonlint.reporter())
		.pipe(jsonlint.failOnError());
	done();
}

function mdlint(done) {
	gulp.src(files ? files : MARKDOWN)
		.pipe(remark({frail: true}));
	done();
}

function pugLint(done) {
	gulp.src(files ? files : PUG)
		.pipe(pugLinter({reporter: pugLintStylish, failAfterError: true}));
	done();
}

function copyExampleScript(done) {
	gulp.src(['js/examples/example-script.js'])
		.pipe(gulp.dest('tmp/js'));
	done();
}

function copyOwnScript(done) {
	gulp.src(['js/examples/own-script.js'])
		.pipe(gulp.dest('tmp/me/js'));
	done();
}

// Copy JSON files to tmp
function copyJSONBuild(done) {
	gulp.src(['responses/*.json'])
		.pipe(gulp.dest('tmp/responses'));
	done();
}

function copyJSONTest(done) {
	gulp.src(['responses/*.json'])
		.pipe(gulp.dest('test_tmp/responses'));
	done();
}

function copyJSTest(done) {
	gulp.src(['dist/cmd-resume.js'])
		.pipe(gulp.dest('test_tmp/js'));
	done();
}

// Copy favicon to tmp
function copyIconsTest(done) {
	gulp.src('favicons/*')
		.pipe(gulp.dest('test_tmp'));
	done();
}

function copyIconsBuild(done) {
	gulp.src('favicons/*')
		.pipe(gulp.dest('tmp'));
	done();
}

// Compile HTML
function compileHTML() {
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	const locals = {
		production: false,
		jquery_script_location: 'node_modules/jquery/dist/jquery.js',
		keyboard_polyfill: 'node_modules/js-polyfills/keyboard.js',
		jquery_terminal_script_location: 'node_modules/jquery.terminal/js/jquery.terminal.js',
		init_script_location: 'js/examples/example-script.js',
		cmd_resume_script_location: 'tmp/js/cmd-resume.js',
		jquery_terminal_stylesheet_location: 'node_modules/jquery.terminal/css/jquery.terminal.css',
		sitename: 'Command Line Résumé',
		favicon_directory: './favicons'
	};
	// jscs:enable requireCamelCaseOrUpperCaseIdentifiers
	return gulp.src('templates/index.pug')
		.pipe(pug({
			locals: locals
		}))
		.pipe(gulp.dest('./'));
}
// Compile JavaScript
function compileReleaseMinified(done) {
	compiledCode('./dist', true, true).on('finish', function() {
		return done();
	});
}

function compileRelease(done) {
	compiledCode('./dist', false, true).on('finish', function() {
		return done();
	});
}

function compileDevelopment(done) {
	compiledCode('./tmp/js', false, false).on('finish', function() {
		return done();
	});
}

const build = gulp.series(compileHTML, compileDevelopment, copyJSONBuild, copyIconsBuild);

const release = gulp.series(compileReleaseMinified, compileRelease);

const sourceCheckDevelopment = gulp.series(compileDevelopment, jscsDevelopment, jsHintDevelopment);

const sourceCheckTools = gulp.series(jshintTools, jscsTools);

const sourceCheckTests = gulp.series(jshintTests, jscsTests);

const sourceCheckUnitTests = gulp.series(jshintUnitTests, jscsUnitTests);
const sourceCheckUITests = gulp.series(jshintUITests, jscsUITests);

const sourceCheck = gulp.series(sourceCheckDevelopment, sourceCheckTools, sourceCheckTests, pugLint);

function compileGHPages() {
	return compiledCode('tmp/js', false, false);
}

function watch() {
	// Example build changes
	gulp.watch(JS_SOURCE, gulp.series(compileDevelopment, jsHintDevelopment, jscsDevelopment));
	gulp.watch(['favicons/*'], copyIconsTest);
	gulp.watch(['responses/*'], copyJSONBuild);

	gulp.watch(['templates/**/*.pug'], gulp.series(pugLint, compileHTML)); // Lint Pug here
	gulp.watch(MARKDOWN, gulp.series(mdlint)); // Run MD Compile here too

	// Pure linting
	gulp.watch(JSON, jsonLint);
	gulp.watch(TOOLS, sourceCheckTools);

	// Unit Testing
	gulp.watch(UNIT_TESTS, gulp.series(jscsUnitTests, jshintUnitTests));

	// UI Testing
	gulp.watch(UI_TESTS, gulp.series(jscsUITests, jshintUITests));
}

// Task for development
const develop = gulp.series(build, gulp.parallel(watch, serve));

module.exports = {
	'default': develop,
	'html': compileHTML,

};
